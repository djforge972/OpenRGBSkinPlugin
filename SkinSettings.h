#ifndef SKINSETTINGS_H
#define SKINSETTINGS_H

#include "json.hpp"
using json = nlohmann::json;

class SkinSettings
{
public:
    static void SaveSettings(json);
    static json LoadSettings();
    static bool CreateSettingsDirectory();

private:
    static const std::string settings_folder;
    static const std::string settings_filename;
};

#endif // SKINSETTINGS_H
